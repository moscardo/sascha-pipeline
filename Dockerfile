FROM rocker/r-ver:3.4.3

RUN apt-get update && apt-get install --no-install-recommends --no-upgrade -y \
libcurl4-openssl-dev \
libxml2-dev \
liblzma-dev \
libbz2-dev \
libz-dev \
build-essential \
ca-certificates \
curl \
git \
libncursesw5-dev \
libncurses5-dev \
make \
zlib1g-dev \    
automake \    
libboost-dev \
&& apt-get clean \
&& rm -rf /var/lib/apt/lists/*

#Setup ENV variables 
ENV BCFTOOLS_BIN="bcftools-1.6.tar.bz2" \
BCFTOOLS_PLUGINS="/usr/local/libexec/bcftools" \
BCFTOOLS_VERSION="1.6"

# htslib 1.6
#RUN cd /opt && git clone git://github.com/samtools/htslib.git 
#RUN cd /opt/htslib && autoreconf \
#&& cd /opt/htslib && ./configure && make && make install



RUN Rscript -e 'install.packages("devtools", dependencies = TRUE)'
RUN Rscript -e 'source("https://bioconductor.org/biocLite.R"); biocLite("Biobase")'
RUN Rscript -e 'source("https://bioconductor.org/biocLite.R"); biocLite("BioGenerics")'
RUN Rscript -e 'source("https://bioconductor.org/biocLite.R"); biocLite("BiocInstaller")'
RUN Rscript -e 'source("https://bioconductor.org/biocLite.R"); biocLite("BiocParallel")'
RUN Rscript -e 'source("https://bioconductor.org/biocLite.R"); biocLite("Biostrings")'
RUN Rscript -e 'source("https://bioconductor.org/biocLite.R"); biocLite("BSgenome")'
RUN Rscript -e 'source("https://bioconductor.org/biocLite.R"); biocLite("BSgenome.Hsapiens.UCSC.hg38")'
RUN Rscript -e 'source("https://bioconductor.org/biocLite.R"); biocLite("DelayedArray")'
RUN Rscript -e 'source("https://bioconductor.org/biocLite.R"); biocLite("fastseg")'
RUN Rscript -e 'source("https://bioconductor.org/biocLite.R"); biocLite("GenomeInfoDb")'
RUN Rscript -e 'source("https://bioconductor.org/biocLite.R"); biocLite("GenomeInfoDbData")'
RUN Rscript -e 'source("https://bioconductor.org/biocLite.R"); biocLite("GenomicAlignments")'
RUN Rscript -e 'source("https://bioconductor.org/biocLite.R"); biocLite("GenomicRanges")'
RUN Rscript -e 'source("https://bioconductor.org/biocLite.R"); biocLite("IRanges")'
RUN Rscript -e 'source("https://bioconductor.org/biocLite.R"); biocLite("Rsamtools")'
RUN Rscript -e 'source("https://bioconductor.org/biocLite.R"); biocLite("S4Vectors")'
RUN Rscript -e 'source("https://bioconductor.org/biocLite.R"); biocLite("SummarizedExperiment")'
RUN Rscript -e 'source("https://bioconductor.org/biocLite.R"); biocLite("XVector")'
RUN Rscript -e 'source("https://bioconductor.org/biocLite.R"); biocLite("zlibbioc")'


# Samtools
WORKDIR /opt
RUN curl -fsSL http://github.com/samtools/samtools/releases/download/1.5/samtools-1.5.tar.bz2 -o /opt/samtools-1.5.tar.bz2 \
&& tar xvjf samtools-1.5.tar.bz2 && cd samtools-1.5 \
&& ./configure && make all all-htslib && make install \
&& rm -rf /opt/samtools-1.5.tar.bz2

# Install BCFTools
#RUN curl -fsSL https://github.com/samtools/bcftools/releases/download/$BCFTOOLS_VERSION/$BCFTOOLS_BIN -o /opt/$BCFTOOLS_BIN \
RUN curl -fsSL https://github.com/samtools/bcftools/releases/download/1.6/bcftools-1.6.tar.bz2 -o /opt/bcftools-1.6.tar.bz2 \
&& tar xvjf /opt/bcftools-1.6.tar.bz2 -C /opt/ \
&& cd /opt/bcftools-1.6 \
&& make \
&& make install \
&& rm -rf /opt/bcftools-1.6.tar.bz2 /opt/bcftools-1.6

#  Python 3.6.4
RUN echo "deb http://ftp.de.debian.org/debian testing main" >> /etc/apt/sources.list
RUN echo 'APT::Default-Release "stable";' | tee -a /etc/apt/apt.conf.d/00local
RUN apt-get update
RUN apt-get -t testing install -y python3.6 python3-pip
ADD python_requirements.txt /opt
RUN pip3 install -r python_requirements.txt
RUN touch /tmp/k
